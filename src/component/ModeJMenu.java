package component;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class ModeJMenu extends JMenu {

	public ModeJMenu() {
		super("Mode");
		JMenu group = new JMenu("グループ");
		group.add(new JMenuItem("平均波形"));
		group.addSeparator();
		group.add(new JMenuItem("ドットマップ"));
		this.add(new JMenuItem("個人"));
		this.addSeparator();
		this.add(group);
		this.setVisible(true);
	}
	
}
