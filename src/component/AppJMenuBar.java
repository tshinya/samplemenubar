package component;

import javax.swing.JMenuBar;

public class AppJMenuBar extends JMenuBar {
	
	public AppJMenuBar() {
		this.add(new DDRJMenu());
		this.add(new FileJMenu());
		this.add(new ModeJMenu());
		this.setVisible(true);
	}

}
