package component;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class FileJMenu extends JMenu {
	
	public FileJMenu() {
		super("File");
		JMenu directory = new JMenu("Directory");
		directory.add(new JMenuItem("Import"));
		directory.addSeparator();
		directory.add(new JMenuItem("Export"));
		
		this.add(new FileItemJMenu());
		this.addSeparator();
		this.add(directory);
		this.setVisible(true);
	}

}