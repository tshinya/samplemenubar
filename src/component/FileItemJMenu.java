package component;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class FileItemJMenu extends JMenu {
	
	public FileItemJMenu() {
		super("File");
		this.add(new JMenuItem("Import"));
		this.addSeparator();
		this.add(new JMenuItem("Export"));
	}
}
