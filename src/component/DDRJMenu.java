package component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class DDRJMenu extends JMenu {
	
	public DDRJMenu() {
		super("DDR");
		JMenuItem quitDDR = new JMenuItem("Quit DDR");
		quitDDR.addActionListener(new QuitActionAdapter());
		this.add(new JMenuItem("About DDR"));
		this.addSeparator();
		this.add(quitDDR);
		this.setVisible(true);
	}
	
	private class QuitActionAdapter implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
		
	}

}