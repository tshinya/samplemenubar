package main;

import javax.swing.JFrame;

import component.AppJMenuBar;

public class MainFrame extends JFrame {
	
	public MainFrame() {
		super("MenuSample");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setJMenuBar(new AppJMenuBar());
	}

}
